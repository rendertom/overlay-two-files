![Overlay Two Files](/Overlay%20Two%20Files.png)

# Overlay Two Files #
Script for Adobe Photoshop to merge two images.

Select bunch of JPEG and PNG files. Script will run and add overlay PNG image on corresponding JPEG file. You also have options to define overlay placement (top-left, top-right etc) as well as overlay margins. Final file will be saved to PSD file.

## Options: ##
* Select multiple images for background
* Select multiple images for overlay
* Define output folder to where final PSDs should be saved
* Set overlays placement (top-left, top-right, bottom-left etc)
* Set margins so overlay image has room to breathe

### Installation ###
Clone or download this repository and place **Overlay Two Files.jsx** script to Photoshop’s Scripts folder:

```Adobe Photoshop CC 20XX -> Presets -> Scripts -> Overlay Two Files.jsx```

Restart Photoshop to access **Overlay Two Files** script from File -> Scripts

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------