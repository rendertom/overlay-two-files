/**********************************************************************************************
    Overlay Two Files.jsx
    Copyright (c) 2016 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
		Select bunch of JPEG and PNG files. Script will run and add overlay
		PNG image on corresponding JPEG file. You also have options to define
		overlay placement (top-left, top-right etc) as well as overlay margins.
		Final file will be saved to PSD file.
**********************************************************************************************/

addOverlay();

function addOverlay() {
	var config = {
		imageFilesArray: [],
		overlayFilesArray: [],
		outputFolder: "",
		placement: 0,
		margin: 20,
	};

	buildUI();

	function buildUI() {
		var btnSize = 24;

		var win = new Window('dialog', "Overlay two files");

		win.spacing = 10;
		win.alignChildren = ["fill", "fill"];

		win.group1 = win.add("group");
		win.group1.alignChildren = ["fill", "fill"];
		win.btnImages = win.group1.add("button", undefined, "Select Images");
		win.btnImages.onClick = function () {
			var imageFiles = selectFiles(true, "jpg");
			if (imageFiles === null) return;
			this.text = imageFiles.length + " Images Selected";
			config.imageFilesArray = imageFiles;
		};

		win.btnOverlays = win.group1.add("button", undefined, "Select Overlays");
		win.btnOverlays.onClick = function () {
			var overlayFiles = selectFiles(true, "png");
			if (overlayFiles === null) return;
			this.text = overlayFiles.length + " Overlays Selected";
			config.overlayFilesArray = overlayFiles;
		};

		win.group2 = win.add("group");
		win.group2.add('statictext', undefined, "Output Folder");
		win.etOutputFolder = win.group2.add('edittext', undefined, config.outputFolder);
		win.etOutputFolder.preferredSize.width = 200;
		win.btnSelectFolder = win.group2.add('button', undefined, "...");
		win.btnSelectFolder.preferredSize.width = btnSize * 2;
		win.btnSelectFolder.onClick = function () {
			var saveFolder = Folder.selectDialog("Select output folder");
			if (saveFolder) {
				win.etOutputFolder.text = saveFolder.fsName;
				config.outputFolder = saveFolder.fsName;
			}
		};

		win.group3 = win.add("group");
		win.group3.add('statictext', undefined, "Overlay Placement");
		win.ddPlacement = win.group3.add('dropdownlist', undefined, ["Center", "Top-Left", "Top-Right", "Bottom-Left", "Bottom-Right"]);
		win.ddPlacement.alignment = ["fill", "fill"];

		win.group4 = win.add("group");
		win.group4.add('statictext', undefined, "Overlay margin");
		win.margin = win.group4.add('edittext', undefined, "20");
		win.margin.alignment = ["fill", "fill"];

		win.group5 = win.add("group");
		win.group5.alignChildren = ["fill", "fill"];
		win.btnCloseWindow = win.group5.add('button', undefined, "Close");
		win.btnCloseWindow.onClick = function () {
			win.close();
		};

		win.btnRunScript = win.group5.add('button', undefined, "Run Script");
		win.btnRunScript.onClick = function () {
			if (config.imageFilesArray.length === 0) {
				alert("No Image Files selected\nPlease select some image files and try again");
				win.btnImages.active = true;
				return;
			} else if (config.overlayFilesArray.length === 0) {
				alert("No Overlay Files selected\nPlease select some overlay files and try again");
				win.btnOverlays.active = true;
				return;
			} else if (config.outputFolder === "" || !Folder(config.outputFolder).exists) {
				alert("Output Folder does not exist\nPlease set Output Folder and try again");
				win.etOutputFolder.active = true;
				return;
			} else if (isNaN(win.margin.text)) {
				alert("Please enter valid margin value");
				win.margin.active = true;
				return;
			}

			// check if same amount of files is selected
			if (config.imageFilesArray.length !== config.overlayFilesArray.length) {
				return alert("You must select same amount of Image files as Overlay files.");
			}

			config.placement = win.ddPlacement.selection.index;
			config.margin = win.margin.text;

			win.close();

			main();
		};

		win.onShow = function () {
			win.ddPlacement.selection = config.placement;
			win.margin.text = config.margin;
		};

		win.show();
	}

	function main() {
		var imageDoc, overlayDoc, qr, docName, dot, saveName, saveFile;

		for (var i = 0, il = config.imageFilesArray.length; i < il; i++) {
			imageDoc = app.open(config.imageFilesArray[i]);
			overlayDoc = app.open(config.overlayFilesArray[i]);

			qr = overlayDoc.layers[0].duplicate(imageDoc);

			overlayDoc.close(SaveOptions.DONOTSAVECHANGES);

			positionLayer(qr);

			docName = decodeURI(activeDocument.name);
			dot = docName.lastIndexOf(".");
			saveName = docName.slice(0, dot);

			saveFile = config.outputFolder + "/" + saveName;
			savePSD(saveFile);

			imageDoc.close();
		}

		alert("Done");
	}

	function positionLayer(layer) {
		try {
			var bounds = layer.bounds;
			var layerWidth = bounds[2].value - bounds[0].value;
			var layerHeigth = bounds[3].value - bounds[1].value;

			var offsetX = bounds[0].value;
			var offsetY = bounds[1].value;

			var docWidth = app.activeDocument.width.value;
			var docHeight = app.activeDocument.height.value;

			var newPos = [];

			if (config.placement === 0) { // center
				newPos[0] = (docWidth / 2) - offsetX - layerWidth / 2;
				newPos[1] = (docHeight / 2) - offsetY - layerHeigth / 2;

			} else if (config.placement === 1) { // top-left
				newPos[0] = config.margin - offsetX;
				newPos[1] = config.margin - offsetY;

			} else if (config.placement === 2) { // top-right
				newPos[0] = docWidth - config.margin - offsetX - layerWidth;
				newPos[1] = config.margin - offsetY;

			} else if (config.placement === 3) { // bottom-left
				newPos[0] = config.margin - offsetX;
				newPos[1] = docHeight - config.margin - offsetY - layerHeigth;

			} else if (config.placement === 4) {
				newPos[0] = docWidth - config.margin - offsetX - layerWidth;
				newPos[1] = docHeight - config.margin - offsetY - layerHeigth;
			}

			layer.translate(newPos[0], newPos[1]);
		} catch (e) {
			alert(e.toString() + "\nLine: " + e.line.toString());
		}
	}

	function savePSD(saveFile) {
		var psdSaveOptions = new PhotoshopSaveOptions();
		psdSaveOptions.embedColorProfile = true;
		psdSaveOptions.alphaChannels = true;
		activeDocument.saveAs(File(saveFile), psdSaveOptions, false, Extension.LOWERCASE);
	}


	function selectFiles(multiSelect, extensionList) {
		var infoMessage = multiSelect ? "Please select multiple files" : "Please select file";
		var filter = ($.os.indexOf("Windows") != -1) ? "*." + extensionList.replace(/ /g, "").replace(/,/g, ";*.") : function (file) {
			var re = new RegExp("\.\(" + extensionList.replace(/,/g, "|").replace(/ /g, "") + ")$", "i");
			if (file.name.match(re) || file.constructor.name === "Folder")
				return true;
		};
		return File.openDialog(infoMessage, filter, multiSelect);
	}
}